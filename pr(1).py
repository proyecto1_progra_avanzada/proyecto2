#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from dlgaux import dlgAux


#ventana principal
class Ventana_inicial():
    def __init__(self):
        self.builder = Gtk.Builder()
        self.builder.add_from_file("./ui/proyecto_2.ui")

        self.window = self.builder.get_object("pr")
        self.window.set_title("Visor de PDB ")
        self.window.maximize()
        self.window.connect("destroy", Gtk.main_quit)

        #botones
        boton_archivo = self.builder.get_object("btnArchivo")
        boton_archivo.connect("clicked", self.boton_archivo_clicked)

        boton_carpeta = self.builder.get_object("btnCarpeta")
        boton_carpeta.connect("clicked", self.boton_carpeta_clicked)

        boton_about = self.builder.get_object("btnAbout")
        boton_about.connect("clicked", self.boton_about_clicked)

        boton_visual = self.builder.get_object("btnVisualiza")
        boton_visual.connect("clicked", self.boton_visualiza)

        self.label_carpeta = self.builder.get_object("labelCarpeta")
        self.window.show_all()


    #funciones de botones
    def boton_archivo_clicked(self, btn=None):
    	print("hola")

    def boton_carpeta_clicked(self, btn=None):

        dialog = Gtk.FileChooserDialog(
        	title="seleccionar una carpeta",
        	parent=self.window,
        	action=Gtk.FileChooserAction.SELECT_FOLDER,
        )
        dialog.add_buttons(
        	Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, "Select", Gtk.ResponseType.OK
        )
        dialog.set_default_size(600, 400)

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
        	self.listar_carpeta(dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
        	print("Cancel clicked")

        dialog.destroy()

    def boton_about_clicked(self, btn=None):
        dlg = dlgAux("dlgAbout")
        dlg.dialogo.run()
        dlg.dialogo.destroy()

    def boton_visualiza(self, btn=None):
        vis = dlgAux("dlgvisual")
        #vis.nombre.set_tex("Visualizador de Archivos PDB")
        vis.dialogo.run()
        vis.dialogo.destroy()


        print("visualiza")


    def listar_carpeta(self, carpeta):
    	print("la carpeta seleccionada es " + carpeta)
    	self.label_carpeta.set_text("Carpeta: " + carpeta)



if __name__ == "__main__":
    Ventana_inicial()
    Gtk.main()
